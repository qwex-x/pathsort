import org.specs2.Specification

class PathSortSpec extends Specification {

  override def is =
    s2"""
        PathSort.sort should
          correct sort m1 $sortM1
          correct sort m2 $sortM2
          correct sort m3 $sortM3
          correct sort m4 $sortM4
          correct sort ci1 $sortCi1
          correct sort m5 $sortM5
          correct sort m6 $sortM6
          correct sort m7 $sortM7
          correct sort m8 $sortM8
          correct sort m9 $sortM9
      """

  private val m1 = Map(
    "id1" -> ("/folder1", 0),
    "id2" -> ("/folder1/item1", 0),
    "id3" -> ("/folder1/item2", 1),
    "id4" -> ("/folder1/item3", 1),
    "id5" -> ("/folder2/item1", 0),
    "id6" -> ("/folder2/item2", 1),
    "id7" -> ("/folder3", 2)
  )
  private val m1Sorted =
    List(
      "/folder1",
      "/folder1/item1",
      "/folder1/item2",
      "/folder1/item3",
      "/folder2/item1",
      "/folder2/item2",
      "/folder3"
    )

  private val m2 = Map(
    "id11" -> ("/folder2/zolder2/item1", 10),
    "id1" -> ("/folder3", 1),
    "id5" -> ("/folder2/item1", 1),
    "id2" -> ("/folder1/item1", 0),
    "id10" -> ("/folder2/zolder2", 0),
    "id8" -> ("/folder1", 2),
    "id4" -> ("/folder1/item3", 1),
    "id6" -> ("/folder2/item2", 0),
    "id7" -> ("/folder4", 0),
    "id3" -> ("/folder1/item2", 1),
    "id9" -> ("/folder2", 10)
  )
  private val m2Sorted =
    List(
      "/folder4",
      "/folder3",
      "/folder1",
      "/folder1/item1",
      "/folder1/item2",
      "/folder1/item3",
      "/folder2",
      "/folder2/item2",
      "/folder2/zolder2",
      "/folder2/zolder2/item1",
      "/folder2/item1"
    )

  private val m3 = Map.empty[String, (String, Int)]
  private val m3Sorted = List.empty[String]

  private val m4 = Map(
    "id11" -> ("/folder2/zolder2/item1", 10),
    "id1" -> ("/folder3", 1),
    "id5" -> ("/folder2/item1", 1),
    "id2" -> ("/folder1/item1", 0),
    "id10" -> ("/folder2/zolder2", 1),
    "id8" -> ("/folder1", 2),
    "id4" -> ("/folder1/item3", 1),
    "id6" -> ("/folder2/item2", 0),
    "id7" -> ("/folder4", 0),
    "id3" -> ("/folder1/item2", 1),
    "id9" -> ("/folder2", 10)
  )
  private val m4Sorted =
    List(
      "/folder4",
      "/folder3",
      "/folder1",
      "/folder1/item1",
      "/folder1/item2",
      "/folder1/item3",
      "/folder2",
      "/folder2/item2",
      "/folder2/item1",
      "/folder2/zolder2",
      "/folder2/zolder2/item1"
    )

  private val m5 = Map(
    "1" -> ("/1", 0),
    "2" -> ("/1/2", 2),
    "3" -> ("/11", 1),
  )

  private val m5Sorted =
    List("/1", "/1/2", "/11")

  private val m6 = Map(
    "1" -> ("/11", 1),
    "2" -> ("/111", 0)
  )

  private val m6Sorted =
    List("/111", "/11")

  private val m7 = Map(
    "1" -> ("/fold", 1),
    "2" -> ("/fold/item4",0),
    "3" -> ("/fold2/", 2)
  )

  private val m7Sorted =
    List("/fold2/", "/fold", "/fold/item4")

  private val m8 = Map(
    "1" -> ("/fold", 1),
    "2" -> ("/fold/item4",0),
    "3" -> ("/fold2", 2)
  )

  private val m8Sorted =
    List("/fold", "/fold/item4", "/fold2")

  private val ci1 = List(
    ("/folder1/item1", 1), ("/folder1/item2", 0)
  )
  private val ci1Sorted =
    List("/folder1/item2", "/folder1/item1")

  private val m9 = Map(
    "1" -> ("/fold", 1),
    "2" -> ("/foldr/item2", 0),
    "3" -> ("/foldr", 0)
  )

    private val m9Sorted =
      List("/foldr", "/foldr/item2", "/fold")

  def sortM1 =
    PathSort.sort(m1) must beEqualTo(m1Sorted)

  def sortM2 =
    PathSort.sort(m2) must beEqualTo(m2Sorted)

  def sortM3 =
    PathSort.sort(m3) must beEqualTo(m3Sorted)

  def sortM4 =
    PathSort.sort(m4) must beEqualTo(m4Sorted)

  def sortCi1 =
    new PathSort(ci1.toMap).sort(ci1.map(_._1)) must beEqualTo(ci1Sorted)

  def sortM5 =
    PathSort.sort(m5) must beEqualTo(m5Sorted)

  def sortM6 =
    PathSort.sort(m6) must beEqualTo(m6Sorted)

  def sortM7 =
    PathSort.sort(m7) must beEqualTo(m7Sorted)

  def sortM8 =
    PathSort.sort(m8) must beEqualTo(m8Sorted)

  def sortM9 =
    PathSort.sort(m9) must beEqualTo(m9Sorted)

}
