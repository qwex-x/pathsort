/** Объект для сортировки путей с частично заданным порядком элементов в каталогах
  *
  *
  * @param catalogInfos отображение: `[путь => порядок элемента в каталоге]`.
  *
  *                     Здесь путь - это строка вида /segment1/segment2/.../segmentN,
  *                     порядок элемента в каталоге - это число.
  *                     Каталог - это путь без последнего сегмента.
  *                     Элемент - это последний сегмент в пути
  */
class PathSort(catalogInfos: Map[String, Int]) {

  /**
    * Разделитель сегментов
    */
  private val pathSeparator = '/'

  /**
   * Назодит длину общего префикса двух строк
   *
   * @param path1
   * @param path2
   * @return длина общего префикса строк `path1` и `[ath2`
   */
  private def findGreatCommonPrefixLength(path1: String, path2: String): Int = {
    path1.view.zip(path2).iterator.takeWhile{
      case (c1, c2) => c1 == c2
    }.iterator.length
  }

  /**
   * Находит в строке `path` конец сегмента, который включает символ с индексом равным greatCommonPrefixLength
   *
   * @param path
   * @param greatCommonPrefixLength
   * @return
   */
  private def findControlPrefix(path: String, greatCommonPrefixLength: Int) = {
    val controlPrefixLength =
      path.indexWhere(_ == pathSeparator, greatCommonPrefixLength)

    if (controlPrefixLength < 0)
      path
    else
      path.slice(0, controlPrefixLength)
  }

  /**
    * Сортирует пути с учетом, заданного в параметре конструктора `catalogInfos`, порядка элементов
    *
    * Результат сравения двух путей `path1` и `path2` , которые имеют вид:
    *
    *`/segment11/segment12/.../segment1N`
    *`/segment21/segment22/.../segment2K`
    *
    * N <= K
    *
    * Равен результату сравнения путей:
    * `/segment11/segment12/.../segment1M`
    * `/segment21/segment22/.../segment2M`,
    *
    * где
    *
    * `/segment11/segment12/.../segment1(M-1) =
    * /segment21/segment22/.../segment2(M-1)``
    *
    * В свою очередь пути
    *
    * `/segment11/segment12/.../segment1M``
    * `/segment21/segment22/.../segment2M`,
    *
    * упорядочиваются по порядку, который либо задан в catalogInfo`, иначе равен `0`.
    *
    * если порядок путей совпадает, то они сранвниваются лексикографически.
    *
    * @param paths список путей
    * @return отсортированный список путей
    **/
  def sort(paths: List[String]): List[String] = {
    paths.sortWith {

      case (path1, path2) =>
        /*Находим длину наибольшего общего префикса для path1 и path2*/
        val greatCommonPrefixLength = findGreatCommonPrefixLength(path1, path2)

        /*Находим контрольне subPath*/
        val controlSubPath1 = findControlPrefix(path1, greatCommonPrefixLength)
        val controlSubPath2 = findControlPrefix(path2, greatCommonPrefixLength)

        /*Находим order для контрольных subPath*/
        val order1 = catalogInfos.getOrElse(controlSubPath1, 0)
        val order2 = catalogInfos.getOrElse(controlSubPath2, 0)

        /*
          Если order'ы равны или один из path равен общему префиксу
          (так будет в случае если он каталог), то сравниваем лексикографически
          пути path1 и path2
          (можно и controlSubPath1 с controlSubPath2. Результат сравнения будет одинаковый).
          Иначе сравниваем по order
        */
        /*Если контрольные subPath совпадают, то значит один из перфиксов совпадает с одним из path и
          это path является subPath для другого. Лексикографически subPath будет меньше чем целый path
        */
        if (
          /*Если длина контрольных subPath равна длине наибольшего общего префикса, то такие subPath равны*/
          greatCommonPrefixLength == controlSubPath1.length && greatCommonPrefixLength == controlSubPath2.length
        ) {
          path1 < path2
        /*Если order'ы равны, сравниваем лексикографически контрольные subPath*/
        } else if (order1 == order2) {
          controlSubPath1 < controlSubPath2
        } else {
        /*Иначе сравниваем по order*/
          order1 < order2
        }
    }
  }

}

object PathSort {

  def sort(idToCatalogInfo: Map[String, (String, Int)]) = {
    val catalogInfos = idToCatalogInfo.values.toMap
    val paths = idToCatalogInfo.values.view.map{case (path, _) => path}.toList

    new PathSort(catalogInfos).sort(paths)
  }

}